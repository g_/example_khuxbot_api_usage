import json, requests, re
from urllib.parse import quote
from difflib import SequenceMatcher

def words():
    #Returns list of all elements in the 2nd level of medals.json
    data = json.load(open(settings.NICKNAME_PATH, "r"))
    return [x.lower() for key in data.keys() for x in data[key]]

def get_real_medal_name(medal):
    #Gets the "Real" name of medals in a list
    #e.g. ibnb --> Belle and Beast Illustrated Ver.
    #Returns a list of strings
    data = json.load(open(settings.NICKNAME_PATH, "r"))
    for key in data.keys():
        if medal.lower() in data[key]:
            return key


def get_medal_data(name):
    url = "http://khuxbot.com{}"
    query = "/api/v1/medal?q=data&medal={}"
    full_url = url.format(query.format(quote(name)))
    response = requests.get(full_url)
    return response.json()


def closest_match(string):
    matcher = {}
    for word in words():
        matcher.update({word:SequenceMatcher(a=word, b=string).quick_ratio()})
    return max(matcher, key=matcher.get)


def sanatise(string):
   return re.sub(r"([\{\}\&\.\[\]])", "", string)
