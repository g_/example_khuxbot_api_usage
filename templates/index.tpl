This is an example on how to use the khuxbot.com API.<br>
The site takes a khux medal nickname, maps it to a real name from khuxbot.com and sends a request for that medal's data

You can find the repository for this code at <a href="https://gitlab.com/g_/example_khuxbot_api_usage">gitlab</a>


<div class="input" id="medal-input">
    <form action="/get_medal">
        Input medal name
        <input type="text" name="medal-name"><br>
        <input type="submit" value="Submit">
    </form>
</div>