from bottle import Bottle, template, request

import settings
import interface

app = Bottle()


@app.route("/")
def index():
    return template("templates/index.tpl")


@app.route("/get_medal")
def medal_request():
    name = request.query["medal-name"]
    name = interface.sanatise(name)
    real_name = interface.get_real_medal_name(name)
    if real_name:
        data = interface.get_medal_data(real_name)
        try:
            data["error"]
        except:
            return template("templates/results.tpl", data = data)
    else:
        close = interface.closest_match(name)
        return """
        Couldn't find your medal, do you mean <strong><a href='/get_medal?medal-name={}'>{}</a></strong>?<br>
        If you're certain this is a correct nickname, please <a href='https://www.reddit.com/message/compose/?to=Leftw'>Contact Me</a>
        """.format(close, close)

if __name__ == "__main__":
    app.run(host=settings.HOST, port=settings.PORT, server="gunicorn", workers=2)
